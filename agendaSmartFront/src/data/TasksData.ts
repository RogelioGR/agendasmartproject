import type { ITask } from "@/interfaces/ITasks";

const TasksData: ITask[] = [
    {
        id: 1,
        title: 'Task 1',
        description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
        completed: false,
        date: new Date(),
        group: {
            id: 2,
            name: "Grupo",
            personID: 2
        }
    },
    {
        id: 2,
        title: 'Task 2',
        description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
        completed: false,
        date: new Date(),
        group: {
            id: 1,
            name: "Personal",
            personID: 1
        }
    },
    {
        id: 3,
        title: 'Task 3',
        description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
        completed: false,
        date: new Date(),
        group: {
            id: 2,
            name: "Grupo",
            personID: 2
        }
    },
    {
        id: 4,
        title: 'Task 4',
        description: 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.',
        completed: true,
        date: new Date(),
        group: {
            id: 1,
            name: "Personal",
            personID: 1
        }
    },
    {
        id: 5,
        title: 'Task 5',
        description: 'Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.',
        completed: false,
        date: new Date(),
        group: {
            id: 2,
            name: "Grupo",
            personID: 1
        }
    },
    {
        id: 6,
        title: 'Task 6',
        description: 'Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.',
        completed: true,
        date: new Date(),
        group: {
            id: 1,
            name: "Personal",
            personID: 1
        }
    },
    {
        id: 7,
        title: 'Task 7',
        description: 'Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur?',
        completed: false,
        date: new Date(),
        group: {
            id: 2,
            name: "Grupo",
            personID: 1
        }
    },
    {
        id: 8,
        title: 'Task 8',
        description: 'Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?',
        completed: true,
        date: new Date(),
        group: {
            id: 1,
            name: "Personal",
            personID: 1
        }
    }
]

export default TasksData;