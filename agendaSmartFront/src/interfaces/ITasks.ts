export interface ITask {
    id: number;
    title: string;
    description: string;
    date: Date;
    completed: boolean;
    group: {
        id: number,
        name: string,
        personID: number,
    };
}