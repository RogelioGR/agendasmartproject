export interface IResponse {
    data: any;
    message: string;
    error: boolean;
}