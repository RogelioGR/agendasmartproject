import { ref } from 'vue'
import type { Ref } from 'vue'
import type { IResponse } from '@/interfaces/IResponse'

const url = 'https://65e8dab54bb72f0a9c508303.mockapi.io/dev/api/Alumnos'

export default class AuthService {
    private token: Ref<string>
    private response: IResponse = {} as IResponse
    constructor() {
        this.token = ref('')
    }

    getToken(): Ref<string> {
        return this.token
    }

    async login(email: string, password: string): Promise<Boolean> {
        const formData = { email, password }

        try {
            const response = await fetch(url + '/login', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(formData)
            })

            const jsonResponse = await response.json()
            if (response.ok) {
                this.token = jsonResponse.token
                return this.token ? true : false
            } else {
                return false
            }
        } catch (error) {
            return false
        }
    }
    async register(name: string, email: string, password: string): Promise<IResponse> {
        const formData = { name, email, password }

        try {
            const response = await fetch(url + '/register', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(formData)
            })
            const jsonResponse = await response.json()
            return jsonResponse
        } catch (error) {
            this.response = {
                data: null,
                message: "Error de red",
                error: true
            }
            return this.response
        }
    }
}
