import { defineStore } from 'pinia'
import { ref, computed } from 'vue'
import AuthService from '@/services/authServices'
import type { IResponse } from '@/interfaces/IResponse'

export const useUserStore = defineStore('user', () => {
  //state
  const user = ref('usuarioporasignar') //Este valor debe asignarse desde el getUser by email pero solo si el usuario ya se logueo
  const token = ref('')
  const service = ref() //Este valor representa a la declaración más no a la inicialización del servicio.
  //actions
  async function login(email: string, password: string): Promise<IResponse> {
    try {
      service.value = new AuthService()
      await service.value.login(email, password)
      token.value = await service.value.getToken()
      if (token.value) {
        localStorage.setItem('myToken', JSON.stringify(token.value))
        return {
          data: token.value,
          message: 'Login correcto',
          error: false
        }
      } else {
        return {
          data: token.value,
          message: 'Credenciales incorrectas',
          error: true
        }
      }
    } catch (error) {
      return {
        data: token.value,
        message: 'Error de red',
        error: true
      }
    }
  }
  function logout() {
    token.value = ''
    localStorage.clear()
  }
  //getters
  const ValidToken = computed(() =>
    (token.value ? true : false) || localStorage.getItem('myToken') ? true : false
  )

  return { login, user, logout, ValidToken }
})
