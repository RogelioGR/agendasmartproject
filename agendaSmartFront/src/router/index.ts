import { createRouter, createWebHistory } from 'vue-router'
import DashboardView from '@/views/DashboardView.vue'
import LoginView from '@/views/LoginView.vue'
import RegisterView from '@/views/RegisterView.vue'
import NotFoundView from '@/views/NotFoundView.vue'
import EditProfileView from '@/views/EditProfileView.vue'
import PapeleraView from '@/views/PapeleraView.vue'
import ActividadView from '@/views/ActividadView.vue'
import GroupView from '@/views/GroupView.vue'
import GroupTasksView from '@/views/GroupTasksView.vue'
const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'dashboard',
      component: DashboardView,
      meta: {
        Auth: false
      }
    },
    {
      path: '/actividad/:id',
      name: 'actividad',
      component: ActividadView,
      meta: {
        Auth: false
      }
    },
    {
      path: '/login',
      name: 'login',
      component: LoginView,
      meta: {
        Auth: false
      }
    },
    {
      path: '/register',
      name: 'register',
      component: RegisterView,
      meta: {
        Auth: false
      }
    },
    {
      path: '/editprofile',
      name: 'editprofile',
      component: EditProfileView,
      meta: {
        Auth: false
      }
    },
    {
      path: '/papelera',
      name: 'papelera',
      component: PapeleraView,
      meta: {
        Auth: false
      }
    },
    {
      path: '/grupos/',
      name: 'grupos',
      component: GroupView,
      meta: {
        Auth: false
      }
    },
    {
      path: '/grupos/:nameGroup',
      name: 'grupo',
      component: GroupTasksView,
      meta: {
        Auth: false
      }
    },
    {
      path: '/:pathMatch(.*)*',
      name: 'notFound',
      component: NotFoundView
    }
  ]
})
router.beforeEach((to, from, next) => {
  const auth = localStorage.getItem('myToken') ? true : false
  const NeedAuth = to.meta.Auth || false

  if (NeedAuth && !auth) {
    next({ name: 'login' })
  } else if (auth && to.name?.toString() == 'login') {
    next({ name: 'home' })
  } else {
    next()
  }
})
export default router
